import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useUserStore = defineStore("user", () => {
  let lastId = 3;
  const editedUser = ref<User>({ id: -1, login: "", name: "", password: "" });
  const Istable = ref(true);
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Aministor", password: "password" },
    { id: 2, login: "user1", name: "User 1", password: "Mypassword" },
    { id: 3, login: "user3", name: "User 3", password: "Mypassword3" },
  ]);
  const login = (loginName: string, password: string) => {
    const index = users.value.findIndex((item) => item.login === loginName);

    if (index >= 0) {
      const user = users.value[index];

      if (user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };

  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id === id);

    users.value.splice(index, 1);
  };
  const clearUser = () => {
    editedUser.value = { id: -1, login: "", name: "", password: "" };
  };
  const dialog = ref(false);

  const saveUser = () => {
    if (editedUser.value.id < 0) {
      editedUser.value.id = lastId++;
      users.value.push(editedUser.value);
    } else {
      const index = users.value.findIndex(
        (item) => item.id === editedUser.value.id
      );
      users.value[index] = editedUser.value;
    }
    dialog.value = false;
    clearUser();
  };

  const editUser = (user: User) => {
    editedUser.value = { ...user };
    dialog.value = true;
  };
  return {
    users,
    deleteUser,
    dialog,
    editedUser,
    clearUser,
    saveUser,
    editUser,
    Istable,
    login,
  };
});
